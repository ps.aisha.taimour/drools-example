package com.baeldung.drools.service;

import com.baeldung.drools.config.DroolsBeanFactory;
import com.baeldung.drools.model.Applicant;
import com.baeldung.drools.model.SuggestedRole;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class SimpleApplicantServiceIntegrationTest {

    private KieSession kSession;

    @Before
    public void setup() {
        Resource resource = ResourceFactory.newClassPathResource("com/baeldung/drools/rules/SuggestApplicant.drl", getClass());
        kSession = new DroolsBeanFactory().getKieSession(resource);
    }

    @Test
    public void whenCriteriaMatching_ThenSuggestManagerRole() throws IOException {
        Applicant applicant = new Applicant("Davis", 37, 1600000.0, 11);

        kSession.insert(applicant);

        SuggestedRole suggestedRole = new SuggestedRole();
        kSession.setGlobal("suggestedRole", suggestedRole);

        kSession.fireAllRules();

        assertEquals("Manager", suggestedRole.getRole());
    }

    @Test
    public void whenCriteriaMatching_ThenSuggestSeniorDeveloperRole() throws IOException {
        Applicant applicant = new Applicant("John", 37, 1200000.0, 8);

        SuggestedRole suggestedRole = new SuggestedRole();
        kSession.insert(applicant);
        kSession.setGlobal("suggestedRole", suggestedRole);
        kSession.fireAllRules();

        assertEquals("Senior developer", suggestedRole.getRole());
    }

    @Test
    public void whenCriteriaMatching_ThenSuggestDeveloperRole() throws IOException {
        Applicant applicant = new Applicant("Davis", 37, 800000.0, 3);

        SuggestedRole suggestedRole = new SuggestedRole();
        kSession.insert(applicant);
        kSession.setGlobal("suggestedRole", suggestedRole);
        kSession.fireAllRules();

        assertEquals("Developer", suggestedRole.getRole());
    }

    @Test
    public void whenCriteriaNotMatching_ThenNoRole() throws IOException {
        Applicant applicant = new Applicant("John", 37, 1200000.0, 5);

        SuggestedRole suggestedRole = new SuggestedRole();
        kSession.insert(applicant);
        kSession.setGlobal("suggestedRole", suggestedRole);
        kSession.fireAllRules();

        assertNull(suggestedRole.getRole());
    }
}
