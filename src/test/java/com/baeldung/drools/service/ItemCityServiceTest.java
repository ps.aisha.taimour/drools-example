package com.baeldung.drools.service;

import com.baeldung.drools.config.DroolsBeanFactory;
import com.baeldung.drools.model.ItemCity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import java.math.BigDecimal;

import static com.baeldung.drools.model.ItemCity.City;
import static com.baeldung.drools.model.ItemCity.Type;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemCityServiceTest {

    private KieSession ksession;

    @BeforeEach
    void setUp() {
        Resource nagpur = ResourceFactory.newClassPathResource("com/baeldung/drools/rules/Nagpur.drl", getClass());
        Resource pune = ResourceFactory.newClassPathResource("com/baeldung/drools/rules/Pune.drl", getClass());
        this.ksession = new DroolsBeanFactory().getKieSession(nagpur, pune);
    }


    @Test
    void whenCriteriaMatching_ThenReturnTheMatchedLocalTax() {
        ItemCity item1 = new ItemCity();
        item1.setPurchaseCity(City.PUNE);
        item1.setTypeofItem(Type.MEDICINES);
        item1.setSellPrice(new BigDecimal(10));
        ksession.insert(item1);

        ItemCity item2 = new ItemCity();
        item2.setPurchaseCity(City.PUNE);
        item2.setTypeofItem(Type.GROCERIES);
        item2.setSellPrice(new BigDecimal(10));
        ksession.insert(item2);

        ItemCity item3 = new ItemCity();
        item3.setPurchaseCity(City.NAGPUR);
        item3.setTypeofItem(Type.MEDICINES);
        item3.setSellPrice(new BigDecimal(10));
        ksession.insert(item3);

        ItemCity item4 = new ItemCity();
        item4.setPurchaseCity(City.NAGPUR);
        item4.setTypeofItem(Type.GROCERIES);
        item4.setSellPrice(new BigDecimal(10));
        ksession.insert(item4);

        ksession.fireAllRules();


        assertEquals(0L, item1.getLocalTax().intValue());
        assertEquals(20L, item2.getLocalTax().intValue());
        assertEquals(0L, item3.getLocalTax().intValue());
        assertEquals(10L, item4.getLocalTax().intValue());
    }
}
