package com.baeldung.drools.service;

import com.baeldung.drools.config.DroolsBeanFactory;
import com.baeldung.drools.model.Applicant;
import com.baeldung.drools.model.SuggestedRole;
import org.kie.api.runtime.KieSession;

public class ApplicantService {

    private final KieSession kieSession;

    public ApplicantService() {
        this.kieSession = new DroolsBeanFactory().getKieSession();
    }

    public SuggestedRole suggestARoleForApplicant(Applicant applicant) {
        SuggestedRole suggestedRole = new SuggestedRole();
        kieSession.insert(applicant);
        kieSession.setGlobal("suggestedRole", suggestedRole);
        kieSession.fireAllRules();
        System.out.println(suggestedRole.getRole());
        return suggestedRole;
    }
}